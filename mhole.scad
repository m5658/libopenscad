// BOF

// NOSTL

module mhole(height, diameter, chamfer, fn = 0) {

    radius = diameter / 2;

    color("pink") {
        cylinder(h = height, d = diameter, center = true);
    }
    
    for(z = [-1, 1]) {
        translate([0, 0, z * height / 2]) {
            color("lightgreen") {
                hull() {
                    rotate_extrude($fn = fn ? fn : $fn) {
                        polygon(points = [
                            [radius, z * -chamfer],
                            [radius, z * chamfer],
                            [radius + chamfer * 2, z * chamfer]
                            ]);
                    }
                }
            }
        }
    }
}




//debug_lib = true;

if(!is_undef(debug_lib)) {


    mhole(height = 40, diameter = 40, chamfer = 5);


}




// EOF
