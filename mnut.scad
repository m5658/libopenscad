use <../libopenscad/mcube.scad>;
use <../libopenscad/mcylinder.scad>;
include <../libthread/threadlib/threadlib.scad>;

    



//cutaway = 1;
debug = true;


default_thread_size = 12;

default_bolt_scale = 0.975;
default_bolt_stretch = 1.25;  // to avoid the need for supporting material by keeping the overhang from being more than 45 degrees
default_bolt_color = "cyan";

default_turns = 2.5;
default_nut_color = "fuchsia";
default_nut_stretch = 1.25;  // to avoid the need for supporting material by keeping the overhang from being more than 45 degrees

default_douter_scale = 1.5;

default_fn = 90;


module mnut(thread_size = default_thread_size, turns = default_turns, douter = undef, douter_scale = default_douter_scale, nut_stretch = default_nut_stretch, color = default_nut_color, $fn = default_fn, align = [0, 0, 0], flutes = true) {


    thread_pattern = str("M", thread_size);
    douter = douter ? douter : thread_size * douter_scale;
    

    specs = thread_specs(str(thread_pattern, "-int"), table=THREAD_TABLE);
    P = specs[0];
    H = (turns + 1) * P * nut_stretch;
    wall_thickness = (douter - thread_size) / 2;

    if(!is_undef(debug)) {
        echo("P:", P);
        echo("H:", H);
        echo("douter:", douter);
        echo("thread_size:", thread_size);
        echo("wall_thickness:", wall_thickness);
        echo(str("nut(thread_pattern: ", thread_pattern, ", turns: ", turns, ", douter: ", douter, ")"));


    }


    module _flutes() {
        
        facets = 6;
        
        color("red") {
            for(z = [0 : 360 / 6 : 359]) {
                rotate([0, 0, z]) {
                    translate([douter/ 2 + thread_size / 6, 0, 0]) {
                        
                        d = thread_size / 2;
                        
                        mcylinder(d = d, h = H);

                        for(z = [-1, 1]) {
                            translate([0, 0, z * H/2 - z * d/16]) {
                                mcylinder(d1 = d - z * (d / 2), d2 = d + z * (d / 2), h = d/2);
                            }
                        }
                        
                        
                    }
                }
            }
        }
    }




    module _nut() {
        translate([0, 0, (P * nut_stretch) / 2]) {             // level the bottom unthreaded part to z plane
            translate([0, 0, -H/2]) {                // center the entire part on z plane
                scale([1, 1, nut_stretch]) {
                    nut(thread_pattern, turns = turns, Douter=douter);
                }
            }
        }
    }


    module _inner_chamfer() {
        for(z = [-1, 1]) {
            translate([0, 0, z * ((H / 2) - thread_size / 16)]) { 
                mcylinder(d1 = thread_size - (z * thread_size), d2 = thread_size + (z * thread_size), h = thread_size, align = [0, 0, 0]);
            }
        }
    }


    module _outer_chamfer() {
        color("pink") {
            hull(){
                for(z = [-1, 1]) {
                    translate([0, 0, z * ((H / 2) - douter / 6)]) { 
                        mcylinder(d1 = douter + (z * douter * 0.25), d2 = douter - (z * douter * 0.25), h = douter - (douter * 0.75), align = [0, 0, z]);
                    }
                }
            }
        }
    }


    translate([align.x * douter / 2, align.y * douter / 2, align.z * H / 2]) {

        intersection() {

            if(!is_undef(cutaway)) {
                color("red") {
                    mcube([douter * 2, douter * 2, H * 2], align = [0, 1, 0]);
                }
            }
        
            color(color) {

                difference() {
                    intersection() {
                        difference() {
                            _nut();
                            _inner_chamfer();
                        } 
                    _outer_chamfer();
                    }



                    if (flutes) {
                        _flutes();
                    }
                }
            }
        }

    }


}







module mbolt(thread_size = default_thread_size, turns = default_turns, bolt_stretch = default_bolt_stretch, bolt_scale = default_bolt_scale, color = default_bolt_color, $fn = default_fn, align = [0, 0, 0]) {


    thread_pattern = str("M", thread_size);
    

    specs = thread_specs(str(thread_pattern, "-int"), table=THREAD_TABLE);
    P = specs[0];
    H = (turns + 1) * P * bolt_stretch;
    if(!is_undef(debug)) {
        echo("P:", P);
        echo("H:", H);
        echo("thread_size:", thread_size);
        echo(str("bolt(thread_pattern: ", thread_pattern, ", turns: ", turns, ")"));
    }

    translate([align.x * thread_size / 2, align.y * thread_size / 2, align.z * H / 2])


    intersection() {
        if(!is_undef(cutaway)) {
            color("red") {
                mcube([thread_size * 2, thread_size * 2, H * 2], align = [0, 1, 0]);
            }
        }


        color(color) {
            translate([0, 0, (P * bolt_stretch) / 2]) {             // level the bottom unthreaded part to z plane
                translate([0, 0, -H/2]) {                // center the entire part on z plane
                    scale([bolt_scale, bolt_scale, bolt_stretch]) {
                        bolt(thread_pattern, turns=turns, higbee_arc=30);
                    }
                }
            }
        }
    }

}

