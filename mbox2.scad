// BOF

// NOSTL

inch = 25.4;

default_inside_height = 50;
default_inside_length = 40;
default_inside_width = 30;
default_inside_radius = 5;
default_wall_thickness = inch / 8;
default_chamfer = default_wall_thickness / 4;


module chamfered_box(dim = [0, 0, 0], align = [0, 0, 0], wall_thickness = default_wall_thickness, chamfer = 0, center = false, xpos = true, xneg = true, ypos = true, yneg = true, zpos = true, zneg = true) {
    
    _chamfer = chamfer;

    final_align = center ? [0, 0, 0] : align;

    translate([(dim.x / 2) * final_align.x, (dim.y / 2) * final_align.y, (dim.z / 2) * final_align.z]) {

echo("final_align.x", final_align.x);


if(xpos) translate([ dim.x / 2,          0,          0]) chamfered_wall([wall_thickness,          dim.y,          dim.z], align = [-1,  0,  0], chamfer = chamfer);
if(xneg) translate([-dim.x / 2,          0,          0]) chamfered_wall([wall_thickness,          dim.y,          dim.z], align = [ 1,  0,  0], chamfer = chamfer);
        
if(ypos) translate([         0,  dim.y / 2,          0]) chamfered_wall([         dim.x, wall_thickness,          dim.z], align = [ 0, -1,  0], chamfer = chamfer);
if(yneg) translate([         0, -dim.y / 2,          0]) chamfered_wall([         dim.x, wall_thickness,          dim.z], align = [ 0,  1,  0], chamfer = chamfer);

if(zpos) translate([         0,          0,  dim.z / 2]) chamfered_wall([         dim.x,          dim.y, wall_thickness], align = [ 0,  0, -1], chamfer = chamfer);
if(zneg) translate([         0,          0, -dim.z / 2]) chamfered_wall([         dim.x,          dim.y, wall_thickness], align = [ 0,  0,  1], chamfer = chamfer);


    }
}



module chamfered_cube(dim = [0, 0, 0], align = [0, 0, 0], chamfer = 0, center = false) {
    _chamfer = chamfer;
    falign = center ? [0, 0, 0] : align;


    translate([(dim.x / 2) * falign.x, (dim.y / 2) * falign.y, (dim.z / 2) * falign.z]) {
        hull() {
            cube([dim.x                 , dim.y - (_chamfer * 2), dim.z - (_chamfer * 2)], center = true);
            cube([dim.x - (_chamfer * 2), dim.y                 , dim.z - (_chamfer * 2)], center = true);
            cube([dim.x - (_chamfer * 2), dim.y - (_chamfer * 2), dim.z                 ], center = true);
        }

    }
}




module chamfered_wall(dim = [0, 0, 0], align = [0, 0, 0], chamfer = 0, center = false, manifold_overlap = true) {

    // only one flat edge at a time as of 2021.07.17

    _chamfer = chamfer;
    falign = center ? [0, 0, 0] : align;
    overlap = manifold_overlap ? 0.01 : 0.00;

    translate([(dim.x / 2) * falign.x, (dim.y / 2) * falign.y, (dim.z / 2) * falign.z]) {
        hull() {
            if(dim.x > 0) {
                cube([dim.x + overlap                 , dim.y + overlap - (_chamfer * 2), dim.z + overlap - (_chamfer * 2)], center = true); // full X
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap                 , dim.z + overlap - (_chamfer * 2)], center = true); // full Y
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap - (_chamfer * 2), dim.z + overlap                 ], center = true); // full Z
            } else {
                cube([-1 * (dim.x + overlap)          , dim.y + overlap - (_chamfer * 2), dim.z + overlap - (_chamfer * 2)], center = true); // full X
                cube([-1 * (dim.x + overlap)          , dim.y + overlap                 , dim.z + overlap - (_chamfer * 2)], center = true); // full Y
                cube([-1 * (dim.x + overlap)          , dim.y + overlap - (_chamfer * 2), dim.z + overlap                 ], center = true); // full Z
            }

            if(dim.y > 0) {
                cube([dim.x + overlap                 , dim.y + overlap - (_chamfer * 2), dim.z + overlap - (_chamfer * 2)], center = true); // full X
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap                 , dim.z + overlap - (_chamfer * 2)], center = true); // full Y
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap - (_chamfer * 2), dim.z + overlap                 ], center = true); // full Z
            } else {
                cube([dim.x + overlap                 , -1 * (dim.y + overlap)                 , dim.z + overlap - (_chamfer * 2)], center = true); // full X
                cube([dim.x + overlap - (_chamfer * 2), -1 * (dim.y + overlap)                 , dim.z + overlap - (_chamfer * 2)], center = true); // full Y
                cube([dim.x + overlap - (_chamfer * 2), -1 * (dim.y + overlap)                 , dim.z + overlap                 ], center = true); // full Z
            }

            if(dim.z > 0) {
                cube([dim.x + overlap                 , dim.y + overlap - (_chamfer * 2), dim.z + overlap - (_chamfer * 2)], center = true); // full X
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap                 , dim.z + overlap - (_chamfer * 2)], center = true); // full Y
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap - (_chamfer * 2), dim.z + overlap                 ], center = true); // full Z
            } else {
                cube([dim.x + overlap                 , dim.y + overlap - (_chamfer * 2), -1 * (dim.z + overlap)], center = true); // full X
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap                 , -1 * (dim.z + overlap)], center = true); // full Y
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap - (_chamfer * 2), -1 * (dim.z + overlap)], center = true); // full Z
            }

        }

    }
}




module radiused_box(
        inside_height = default_inside_height, 
        inside_length = default_inside_length, 
        inside_width = default_inside_width, 
        inside_radius = default_inside_radius, 
        wall_thickness = default_wall_thickness, 
        color = "pink", 
        wall_color = "sandybrown",
        corner_color = "mediumpurple",
        chamfer = -1, 
        align = [0, 0, 0], 
        center = false, 
        manifold_overlap = true
    ) {


    overlap = manifold_overlap ? 0.01 : 0.00;

    cchamfer = (chamfer == -1) ? wall_thickness / 4 : chamfer;
    falign = center ? [0, 0, 0] : align;

    translate([
                (inside_width  / 2 + wall_thickness) * falign.x,
                (inside_length / 2 + wall_thickness) * falign.y,
                inside_height / 2 * falign.z
              ]) {

        x_offset = inside_width/2;
        y_offset = inside_length/2;
                  
        // walls
        color(wall_color) {
            for(x = [-1 , 1]) {
                translate([x * x_offset, 0, 0]) {
                    chamfered_wall(dim = [wall_thickness, -(inside_length - (inside_radius - overlap) * 2), inside_height], align = [x, 0, 0], chamfer = cchamfer);
                }
            }
            for(y = [-1 , 1]) {
                translate([0, y * y_offset, 0]) {
                    chamfered_wall(dim = [-(inside_width - (inside_radius - overlap) * 2), wall_thickness, inside_height], align = [0, y, 0], chamfer = cchamfer);
                }
            }
        }

        // corners
        color(corner_color) {



            for(x = [-1, 1]) {
                for(y = [-1, 1]) {
                    

                    translate([x * (x_offset - inside_radius), y * (y_offset - inside_radius), 0]) {

                        /*
                            "x: ", -1, "y", -1, "rotation", 180
                            "x: ", -1, "y",  1, "rotation",  90
                            "x: ",  1, "y", -1, "rotation", -90
                            "x: ",  1, "y",  1, "rotation",   0
                        */
                        rotation = (x < 0 ? 90 : 0) + (y < 0 ? (x * -90) : 0);
                        rotate([0, 0, rotation]) {

                            rotate_extrude(angle = 90) {
                                translate([inside_radius + wall_thickness/2, 0, 0]) {
                                    hull() {
                                        square([wall_thickness - cchamfer * 2, inside_height], center = true);
                                        square([wall_thickness, inside_height - cchamfer * 2], center = true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}



debug_mbox = true;



if(!is_undef(debug_mbox) && debug_mbox) {

//    $fn = 150;
    radiused_box();



    chamfered_cube(dim = [default_inside_width, default_inside_length, default_inside_height], chamfer = default_inside_radius, align = [0, 0, -1]);


    // box test in the LR quadrant
    //
    translate([20, -50, 0]) {
        chamfered_cube(dim = [20, 20, 20], chamfer = 1);

        translate([30, 30, 0]) {
            chamfered_cube(dim = [15, 25, 30], chamfer = 3);
        }
    }

    translate([50, -70, 0]) {
         chamfered_wall(dim = [30, 10, 20], align = [0, 0, 0], chamfer = 2, center = true, manifold_overlap = true);

    }

    translate([0, -90, 0]) {
        color("palevioletred")
            chamfered_box(dim = [20, 30, 40], wall_thickness = default_wall_thickness, chamfer = 1, align = [-1, 0, 0], zpos = false);

    }


    translate([0, -125, 0]) {
        color("lightskyblue")
            chamfered_box(dim = [20, 30, 40], wall_thickness = default_wall_thickness, chamfer = 1, align = [ 1, 0, 0], xpos = false);

    }
    
translate([-50, -120, 0]) {
color("lightblue")
    chamfered_box(dim = [30,30,30], wall_thickness = default_wall_thickness, chamfer = 0, center = false, xpos = false, xneg = false);

minkowski() {
    chamfered_box(dim = [28,28,28], wall_thickness = default_wall_thickness - 2, chamfer = 0, center = false, xpos = false, xneg = false, align = [-1, 0, 0]);
    sphere(1);
}
}


}

// EOF
